package com.example.mystations

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.mystations.models.StationModel
import com.example.mystations.retrofit.RetrofitHelper
import com.example.mystations.retrofit.RetrofitService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class StationsListViewModel() : ViewModel() {
    var liveDataStationsList: MutableLiveData<List<StationModel>> = MutableLiveData()

    init {
        getStationsData()
    }

    fun getLiveData(): MutableLiveData<List<StationModel>> {
        return liveDataStationsList
    }

    fun getStationsData() {
        val requestApi = RetrofitHelper.getRetrofitHelper().create(RetrofitService::class.java)
        requestApi.getStationsList().enqueue(object : Callback<List<StationModel>> {
            override fun onResponse(
                call: Call<List<StationModel>>,
                response: Response<List<StationModel>>
            ) {
                if(response?.body() != null) {
                    Log.d("ReponseApiCall: ", response.body().toString())
                    liveDataStationsList.postValue(response.body())
                }
            }

            override fun onFailure(call: Call<List<StationModel>>, t: Throwable) {
                Log.d("failure: ", t.toString())
            }
        })
    }
    // TODO: Implement the ViewModel
}