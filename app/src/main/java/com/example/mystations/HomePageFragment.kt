package com.example.mystations

import android.Manifest
import android.content.pm.PackageManager
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.navigation.Navigation

class HomePageFragment : Fragment() {

    companion object {
        fun newInstance() = HomePageFragment()
    }

    private lateinit var viewModel: HomePageViewModel
    private lateinit var buttonAccessStationsList: Button
    private lateinit var requestPermissionsLauncher: ActivityResultLauncher<Array<String>>
    private var isInternetPermissionGranted = false
    private var isFineLocationPermissionGranted = false
    private var isCoarseLocationPermissionGranted = false

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_home_page, container, false)
        buttonAccessStationsList = view.findViewById(R.id.button_access_stationsList)

        if(isAllPermissionsChecked()) {
            buttonAccessStationsList.isVisible = false
        }

        requestPermissionsLauncher = registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) {
                permissions ->
            isInternetPermissionGranted = permissions[Manifest.permission.INTERNET] ?: isInternetPermissionGranted
            isFineLocationPermissionGranted = permissions[Manifest.permission.ACCESS_FINE_LOCATION] ?: isFineLocationPermissionGranted
            isCoarseLocationPermissionGranted = permissions[Manifest.permission.ACCESS_COARSE_LOCATION] ?: isCoarseLocationPermissionGranted

            if(isAllPermissionsChecked()) {
                Navigation.findNavController(view).navigate(R.id.action_homePageFragment_to_stationsListFragment)
            }
        }
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if(isAllPermissionsChecked()) {
            Handler(Looper.getMainLooper()).postDelayed({
                    Navigation.findNavController(view).navigate(R.id.action_homePageFragment_to_stationsListFragment)
            }, 3000)
        } else {
            buttonAccessStationsList.setOnClickListener {
                requestPermission()
            }
        }
    }

    private fun requestPermission() {
        val context = context;
        if (context !== null) {
            isInternetPermissionGranted = ContextCompat.checkSelfPermission(
                context, Manifest.permission.INTERNET) == PackageManager.PERMISSION_GRANTED
            isFineLocationPermissionGranted = ContextCompat.checkSelfPermission(
                context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
            isCoarseLocationPermissionGranted = ContextCompat.checkSelfPermission(
                context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED

            val permissionRequestList : MutableList<String> = ArrayList()

            if(!isInternetPermissionGranted) {
                permissionRequestList.add(Manifest.permission.INTERNET)
            }

            if(!isFineLocationPermissionGranted) {
                permissionRequestList.add(Manifest.permission.ACCESS_FINE_LOCATION)
            }

            if(!isCoarseLocationPermissionGranted) {
                permissionRequestList.add(Manifest.permission.ACCESS_COARSE_LOCATION)
            }

            if(permissionRequestList.isNotEmpty()) {
                requestPermissionsLauncher.launch(permissionRequestList.toTypedArray())
            }
        }
    }

    private fun isAllPermissionsChecked(): Boolean {
        val context = context;
        return context !== null &&
                ContextCompat.checkSelfPermission(context, Manifest.permission.INTERNET) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(HomePageViewModel::class.java)
        // TODO: Use the ViewModel
    }

}