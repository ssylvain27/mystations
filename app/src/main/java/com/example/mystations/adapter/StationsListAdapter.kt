package com.example.mystations.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.mystations.R
import com.example.mystations.StationsListFragmentDirections
import com.example.mystations.models.StationModel

class StationsListAdapter: RecyclerView.Adapter<StationsListAdapter.ViewHolder>() {

    private var stationsList: List<StationModel>? = null

    fun setStationsList(stationsList: List<StationModel>?) {
        this.stationsList = stationsList
    }

    fun getStationsList(): List<StationModel>? {
        return stationsList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item_station, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(stationsList?.get(position)!!)
        holder.currentPosition = position
        holder.stationsList = stationsList as List<StationModel>
    }

    override fun getItemCount(): Int {
        return stationsList?.size ?: 0
    }

    class ViewHolder(view: View): RecyclerView.ViewHolder(view) {
        val stationName = view.findViewById<TextView>(R.id.station_name)
        val townName = view.findViewById<TextView>(R.id.town_name)
        val distance = view.findViewById<TextView>(R.id.distance)
        val passengerStatus = view.findViewById<ImageView>(R.id.passenger_status)
        var currentPosition = 0
        lateinit var stationsList: List<StationModel>

        init {
            view.setOnClickListener {
                val action = StationsListFragmentDirections.actionStationsListFragmentToStationDetailsFragment(stationsList[currentPosition])
                view.findNavController().navigate(action)
            }
        }

        fun bind(data: StationModel) {
            stationName.text = data.fields?.libelle
            townName.text = data.fields?.commune
            distance.text = String.format("%.2f", data.distance) + " km"
            if(data.fields?.voyageurs.equals("O")) {
                passengerStatus.setImageResource(R.drawable.ic_baseline_circle_12)
            } else {
                passengerStatus.setImageResource(R.drawable.ic_baseline_circle_12_red)
            }
        }

    }
}