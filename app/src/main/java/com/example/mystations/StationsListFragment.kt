package com.example.mystations

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.airbnb.lottie.LottieAnimationView
import com.example.mystations.adapter.StationsListAdapter

class StationsListFragment : Fragment() {

    companion object {
        fun newInstance() = StationsListFragment()
    }

    private lateinit var viewModel: StationsListViewModel
    private lateinit var locationViewModel: LocationViewModel
    private lateinit var stationListAdapter: StationsListAdapter
    private lateinit var loader: LottieAnimationView


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        activity?.setTitle(R.string.stationListTitleScreen)
        val view = inflater.inflate(R.layout.fragment_stations_list, container, false)

        loader = view.findViewById(R.id.loader)
        val stationsListRecyclerView: RecyclerView = view.findViewById(R.id.stationsListRecyclerView)

        stationsListRecyclerView.layoutManager = LinearLayoutManager(context)
        stationListAdapter = StationsListAdapter()
        stationsListRecyclerView.adapter = stationListAdapter

        return view

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        startLoader()
        viewModel = ViewModelProvider(this).get(StationsListViewModel()::class.java)

        viewModel.getLiveData().observe(viewLifecycleOwner, Observer {
            if(it != null) {
                stationListAdapter.setStationsList(it)
            } else {
               Log.d("Error List", "Error get List")
            }
        })

        locationViewModel = ViewModelProvider(this).get(LocationViewModel::class.java)
        locationViewModel.getLocationData().observe(viewLifecycleOwner, Observer {
            val coordinates = it
            val stationsList = stationListAdapter.getStationsList()

            stationsList?.forEach { stationModel ->
                stationModel.distance = locationViewModel.getLocationData().differenceBetween(
                    coordinates.latitude,
                    coordinates.longitude,
                    stationModel.fields?.c_geo?.get(0)!!.toDouble(),
                    stationModel.fields?.c_geo?.get(1)!!.toDouble()
                )
            }
            val sortedStationsList = stationsList?.sortedBy {
                it.distance
            }

            if(!sortedStationsList.isNullOrEmpty()) {
                endLoader()
            } else {
                Log.d("listStatus","Not sorted yet")
            }
            stationListAdapter.setStationsList(sortedStationsList)
            stationListAdapter.notifyDataSetChanged()

        })
    }

    fun startLoader() {
        loader.isVisible = true
    }

    fun endLoader() {
        loader.isVisible = false
    }

}