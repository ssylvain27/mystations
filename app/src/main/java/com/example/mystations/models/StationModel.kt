package com.example.mystations.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class StationModel(
    val recordid: String?,
    val fields: StationField?,
    val record_timestamp: String?,
    var distance: Double?
): Parcelable

@Parcelize
data class StationField(
    val commune: String?,
    val voyageurs: String?,
    val fret: String?,
    val libelle: String?,
    val departemen: String?,
    val c_geo: Array<String>?
    ): Parcelable