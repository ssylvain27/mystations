package com.example.mystations.models

data class LocationModel(
    val longitude: Double,
    val latitude: Double
)