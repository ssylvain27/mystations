package com.example.mystations

import android.app.Application
import androidx.lifecycle.AndroidViewModel

class LocationViewModel (application: Application): AndroidViewModel(application) {
    private val location = Location(application)

    fun getLocationData() = location

}