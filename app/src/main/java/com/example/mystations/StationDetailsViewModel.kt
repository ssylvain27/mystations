package com.example.mystations

import com.example.mystations.models.StationModel

class StationDetailsViewModel (stationModel: StationModel) {

    //Station geo
    val stationName = stationModel.fields?.libelle
    val townName = stationModel.fields?.commune
    val departmentName = stationModel.fields?.departemen
    val distanceFromStationDetails = String.format("%.2f", stationModel.distance) + " km"
    val stationLatitude = stationModel.fields?.c_geo?.get(0)
    val stationLongitude = stationModel.fields?.c_geo?.get(1)

    //Further information
    var passengersStatus = stationModel.fields?.voyageurs
    val fretStatus = stationModel.fields?.fret
}

