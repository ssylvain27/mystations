package com.example.mystations.retrofit

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitHelper {

    companion object {
        val BASE_URL = "https://ressources.data.sncf.com/explore/dataset/liste-des-gares/"

        fun getRetrofitHelper(): Retrofit {
            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }
    }
}