package com.example.mystations.retrofit

import com.example.mystations.models.StationModel
import retrofit2.Call
import retrofit2.http.GET

interface RetrofitService {

    @GET("download?format=json&timezone=Europe/Berlin&use_labels_for_header=false")
    fun getStationsList(): Call<List<StationModel>>
}